#!/bin/sh

if [ -z "${ALLIANCEAUTH_ENTRYPOINT_QUIET_LOGS:-}" ]; then
	exec 3>&1
else
	exec 3>/dev/null
fi

__log() {
	log_level="$1"
	shift
	printf '%s: entrypoint: %s\n' "$log_level" "$*" >&3
}
log_err() {
	__log err "$@"
	exit 1
}
log_warn() {
	__log warning "$@"
}
log_notice() {
	__log notice "$@"
}
log_info() {
	__log info "$@"
}
