; Database configuration
[database]
; Only tested with MySQL at the moment
lib        = %%MUMBLE_AUTHENTICATOR_DATABASE_LIB%%
name       = %%MUMBLE_AUTHENTICATOR_DATABASE_NAME%%
user       = %%MUMBLE_AUTHENTICATOR_DATABASE_USER%%
password   = %%MUMBLE_AUTHENTICATOR_DATABASE_PASSWORD%%
prefix     = %%MUMBLE_AUTHENTICATOR_DATABASE_PREFIX%%
host       = %%MUMBLE_AUTHENTICATOR_DATABASE_HOST%%
port       = %%MUMBLE_AUTHENTICATOR_DATABASE_PORT%%

; Player configuration
[user]
; If you do not already know what it is just leave it as it is
id_offset       = %%MUMBLE_AUTHENTICATOR_USER_ID_OFFSET%%

; Reject users if the authenticator experiences an internal error during authentication
reject_on_error = %%MUMBLE_AUTHENTICATOR_USER_REJECT_ON_ERROR%%

; If enabled, textures are automatically set as player's EvE avatar for use on overlay.
avatar_enable = %%MUMBLE_AUTHENTICATOR_USER_AVATAR_ENABLE%%

; Get EvE avatar images from this location. {charid} will be filled in.
ccp_avatar_url = %%MUMBLE_AUTHENTICATOR_USER_CCP_AVATAR_URL%%


; Ice configuration
[ice]
host            = %%MUMBLE_AUTHENTICATOR_ICE_HOST%%
port            = %%MUMBLE_AUTHENTICATOR_ICE_PORT%%
slice           = %%MUMBLE_AUTHENTICATOR_ICE_SLICE%%
secret          = %%MUMBLE_AUTHENTICATOR_ICE_SECRET%%
watchdog        = %%MUMBLE_AUTHENTICATOR_ICE_WATCHDOG%%
endpoint        = %%MUMBLE_AUTHENTICATOR_ICE_ENDPOINT%%

; Murmur configuration
[murmur]
; List of virtual server IDs
servers      = %%MUMBLE_AUTHENTICATOR_MURMUR_SERVERS%%


; Logging configuration
[log]
; Available loglevels: 10 = DEBUG (default) | 20 = INFO | 30 = WARNING | 40 = ERROR
level   = %%MUMBLE_AUTHENTICATOR_LOG_LEVEL%%

; Log file
file    = %%MUMBLE_AUTHENTICATOR_LOG_FILE%%


[iceraw]
Ice.ThreadPool.Server.Size = %%MUMBLE_AUTHENTICATOR_ICERAW_ICE_THREADPOOL_SERVER_SIZE%%


[idlerhandler]
; An AFK or Idle handler to move people to a set "AFK" Channel
; Enable the Feature
enabled = %%MUMBLE_AUTHENTICATOR_IDLERHANDLER_ENABLED%%

; Mumble idlesecs Threshold (Seconds) until a user is considered Idle,
time = %%MUMBLE_AUTHENTICATOR_IDLERHANDLER_TIME%%

; Interval(Seconds) to run the Idler Handler at
interval = %%MUMBLE_AUTHENTICATOR_IDLERHANDLER_INTERVAL%%

; Channel to move Idlers to
channel = %%MUMBLE_AUTHENTICATOR_IDLERHANDLER_CHANNEL%%

; Channels for IdlerHandler to Ignore, Comma separated channel IDs, denylist overwrites allowlist
denylist = [%%MUMBLE_AUTHENTICATOR_IDLERHANDLER_DENYLIST%%]

; Channels for IdlerHandler to Process, Comma separated channel IDs
allowlist = [%%MUMBLE_AUTHENTICATOR_IDLERHANDLER_ALLOWLIST%%]
