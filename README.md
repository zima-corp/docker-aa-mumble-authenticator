⚠️ Deprecated Container:
This Docker container is an outdated version of the Mumble Authenticator and is no longer maintained.
Please use [the new repository](https://gitlab.com/eveo7/mumble-authenticator-templinks) to build and run the updated version of the application with the latest dependencies and features.
