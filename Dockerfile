FROM python:3.10-slim-bookworm

ARG DEBIAN_FRONTEND=noninteractive
ARG MUMBLE_AUTHENTICATOR_REPO="https://gitlab.com/eveo7/mumble-authenticator-templinks.git"
ARG MUMBLE_AUTHENTICATOR_REPO_TAG="v1.1.0p1"
ENV MUMBLE_AUTHENTICATOR_USER="mumbleauth"
ENV MUMBLE_AUTHENTICATOR_GROUP="mumbleauth"
ENV MUMBLE_AUTHENTICATOR_UID="10000"
ENV MUMBLE_AUTHENTICATOR_GID="10000"

ENV MUMBLE_AUTHENTICATOR_INSTALL_DIR="/mumbleauth"

RUN \
  set -eux; \
  mkdir ${MUMBLE_AUTHENTICATOR_INSTALL_DIR}; \
  cd ${MUMBLE_AUTHENTICATOR_INSTALL_DIR}; \
  groupadd -r ${MUMBLE_AUTHENTICATOR_GROUP} -g ${MUMBLE_AUTHENTICATOR_GID}; \
  useradd -l -r -g ${MUMBLE_AUTHENTICATOR_GROUP} -d ${MUMBLE_AUTHENTICATOR_INSTALL_DIR} -s /bin/bash -u ${MUMBLE_AUTHENTICATOR_UID} ${MUMBLE_AUTHENTICATOR_USER}; \
  savedAptMark="$(apt-mark showmanual)"; \
  apt-get update; \
  apt-get install -y --no-install-recommends \
  default-libmysqlclient-dev \
  g++ \
  gcc \
  git \
  libbz2-dev \
  libmariadb-dev \
  libssl-dev \
  pkgconf \
  ; \
  apt-mark auto '.*' > /dev/null; \
  [ -z "$savedAptMark" ] || apt-mark manual $savedAptMark > /dev/null; \
  apt-get install -y --no-install-recommends \
  gosu \
  libmariadb3 \
  tini \
  ; \
  python --version; \
  pip --version; \
  git clone --branch ${MUMBLE_AUTHENTICATOR_REPO_TAG} --single-branch --depth 1 ${MUMBLE_AUTHENTICATOR_REPO} .; \
  pip install --no-cache-dir --disable-pip-version-check -r requirements.txt; \
  chown -R ${MUMBLE_AUTHENTICATOR_USER}:${MUMBLE_AUTHENTICATOR_GROUP} ${MUMBLE_AUTHENTICATOR_INSTALL_DIR}; \
  apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
  apt-get autoclean -y; \
  rm -rf /var/lib/apt/lists/*; \
  rm -rf /var/cache/apt/archives/*; \
  rm -rf *.md; \
  rm -rf .git*;

COPY . /

WORKDIR ${MUMBLE_AUTHENTICATOR_INSTALL_DIR}

ENTRYPOINT ["tini", "--", "/docker-entrypoint.sh"]

CMD []
