#!/bin/sh -e

. /docker-entrypoint-common.sh

if [ "$#" -eq 0 -o "${1#-}" != "$1" -o "$1" = "cron" ]; then
	if find "/docker-entrypoint.d/" -mindepth 1 -maxdepth 1 -type f -print -quit 2>/dev/null | read v; then
		log_info "/docker-entrypoint.d/ is not empty, will attempt to perform configuration"

		log_info "looking for shell scripts in /docker-entrypoint.d/"
		find "/docker-entrypoint.d/" -follow -type f -print | sort -V | while read -r f; do
			case "$f" in
			*.sh)
				if [ -x "$f" ]; then
					log_info "launching $f"
					"$f"
				else
					# warn on shell scripts without exec bit
					log_warn "ignoring $f, not executable"
				fi
				;;
			*) log_info "ignoring $f" ;;
			esac
		done

		log_info "configuration complete; ready for start up"
	else
		log_info "no files found in /docker-entrypoint.d/, skipping configuration"
	fi
fi

if [ "$#" -eq 0 ] || [ "${1#-}" != "$1" ]; then
	set -- python authenticator.py --app "$@"
fi

if [ "$(id -u)" = "0" ]; then
	exec gosu $MUMBLE_AUTHENTICATOR_USER:$MUMBLE_AUTHENTICATOR_GROUP "$@"
fi

exec "$@"
